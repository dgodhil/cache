package strategy;

import java.util.Comparator;
import java.util.Map;

/**
 * Вытеснение давно неиспользуемых
 */
public class LeastRecentlyUsedStrategyImpl<K>  implements Strategy<K> {

    @Override
    public Map<K, Long> updateCacheInfo(K key, Map<K, Long> cacheInfo) {
        cacheInfo.put(key, System.nanoTime());
        return cacheInfo;
    }

    @Override
    public K cacheElementForRemove(Map<K, Long> cacheInfo) {
        return cacheInfo.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).map(Map.Entry::getKey).findFirst().orElseGet(null);
    }
}
