package strategy;

import java.util.Map;

/**
 * MFU - Наиболее недавно использовавшийся
 */
public class MostRecentlyUsedStrategyImpl<K> implements Strategy<K>  {

    @Override
    public Map<K, Long> updateCacheInfo(K key, Map<K, Long> cacheInfo) {
        cacheInfo.clear();
        cacheInfo.put(key, 0L);
        return cacheInfo;
    }

    @Override
    public K cacheElementForRemove(Map<K, Long> cacheInfo) {
        return cacheInfo.entrySet().iterator().next().getKey();
    }
}
