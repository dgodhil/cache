package strategy;

import java.util.Comparator;
import java.util.Map;

/**
 * LFU - Наименее часто используемый
 */
public class LeastFrequentlyUsedStrategyImpl<K>  implements Strategy<K>  {

    @Override
    public Map<K, Long> updateCacheInfo(K key, Map<K, Long> cacheInfo) {
        long value = 0;
        if (!cacheInfo.isEmpty() && cacheInfo.containsKey(key)) {
            value = cacheInfo.get(key) + 1;
        }
        cacheInfo.put(key, value);
        return cacheInfo;
    }

    @Override
    public K cacheElementForRemove(Map<K, Long> cacheInfo) {
        return cacheInfo.entrySet().stream().sorted(Comparator.comparing(Map.Entry::getValue)).map(Map.Entry::getKey).findFirst().orElseGet(null);
    }
}
