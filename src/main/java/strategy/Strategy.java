package strategy;

import java.util.Map;

public interface Strategy<K> {

    Map<K, Long> updateCacheInfo(K key, Map<K, Long> cacheInfo);

    K cacheElementForRemove(Map<K, Long> cacheInfo);

}
