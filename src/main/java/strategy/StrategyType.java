package strategy;

/**
 * LRU - Least Recently Used
 * MRU - Most Recently Used
 * LFU - Least Frequently Used
 */
public enum  StrategyType {
    LRU,
    MRU,
    LFU;
}
