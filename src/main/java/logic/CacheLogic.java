package logic;

import java.io.IOException;

public interface CacheLogic<K, V> {

    void put(K key, V value);

    V get(K key);

    void removeElement(K key);

    void clear() throws IOException;

    boolean cacheIsEmpty();

    boolean hasPlace();

    boolean cacheContainsKey(K key);
}
