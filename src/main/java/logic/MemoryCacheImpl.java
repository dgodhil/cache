package logic;

import java.util.LinkedHashMap;
import java.util.Map;

public class MemoryCacheImpl<K, V> implements CacheLogic<K, V> {

    private final Map<K, V> cache;
    private final int cacheSize;

    public MemoryCacheImpl(int cacheSize) {
        this.cacheSize = cacheSize;
        this.cache = new LinkedHashMap<>(cacheSize);
    }

    @Override
    public void put(K key, V value) {
        cache.put(key, value);
    }

    @Override
    public V get(K key) {
        return cache.getOrDefault(key, null);
    }

    @Override
    public void removeElement(K key) {
        if (!cache.containsKey(key)) {
            return;
        }
        cache.remove(key);
    }

    @Override
    public void clear() {
        cache.clear();
        System.out.println("Memory cache is clear");
    }

    @Override
    public boolean cacheIsEmpty() {
        return cache.isEmpty();
    }

    @Override
    public boolean hasPlace() {
        return cache.isEmpty() || cache.size() < cacheSize;
    }

    @Override
    public boolean cacheContainsKey(K key) {
        return cache.containsKey(key);
    }

}