package logic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

public class FileCacheImpl<K, V> implements CacheLogic<K, V> {

    private final Path path;
    private final Map<K, V> cache;
    private final int cacheSize;

    public FileCacheImpl(int cacheSize) throws IOException {
        this.cacheSize = cacheSize;
        this.cache = new HashMap<>(cacheSize);
        this.path = Files.createTempDirectory("cachePath");
    }

    @Override
    public void put(K key, V value) {
        if (cache.containsKey(key) && cache.get(key).equals(value)) {
            System.out.println(format("Cache already got this key %s", key));
        }
        try (FileOutputStream outputStream = new FileOutputStream(new File(path + File.separator + key))) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(value);
            objectOutputStream.flush();
            objectOutputStream.close();

            cache.put(key, value);
        } catch (Exception e) {
            System.out.println(format("Can't write file with name %s", key));
        }
    }

    @Override
    public V get(K key) {
        if (cache.containsKey(key)) {
            try (FileInputStream inputStream = new FileInputStream(new File(path + File.separator + key))) {
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                V result = (V) objectInputStream.readObject();
                objectInputStream.close();
                return result;
            } catch (Exception e) {
                System.out.println();
            }
        }
        System.out.println(format("File %s not in cache", key));
        return null;
    }

    @Override
    public void removeElement(K key) {
        if (!cache.containsKey(key)) {
            System.out.println(format("File %s not in cache", key));
            return;
        }
        File file = new File(path + File.separator + key);
        if (file.delete()) {
            System.out.println(format("File %s deleted", key));
            cache.remove(key);
        } else {
            System.out.println(format("Cant delete file %s", key));
        }
    }

    @Override
    public void clear() {
        try {
            Files.walk(path)
                    .map(Path::toFile)
                    .forEach(f -> {
                        if (f.delete()) {
                            System.out.println(format("File %s deleted", f.getName()));
                        } else {
                            System.out.println(format("File %s deleted", f.getName()));
                        }
                    });
            cache.clear();
        } catch (IOException e) {
            System.out.println(format("Cant read directory %s", path.getFileName()));
        }
        System.out.println("File cache is clear");
    }

    @Override
    public boolean cacheIsEmpty() {
        return cache.isEmpty();
    }

    @Override
    public boolean hasPlace() {
        return cache.isEmpty() || cache.size() < cacheSize;
    }

    @Override
    public boolean cacheContainsKey(K key) {
        return cache.containsKey(key);
    }

}