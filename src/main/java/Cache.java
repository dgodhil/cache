import logic.CacheLogic;
import logic.FileCacheImpl;
import logic.MemoryCacheImpl;
import strategy.LeastFrequentlyUsedStrategyImpl;
import strategy.LeastRecentlyUsedStrategyImpl;
import strategy.MostRecentlyUsedStrategyImpl;
import strategy.Strategy;
import strategy.StrategyType;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.lang.String.format;

public class Cache<K, V> implements CacheLogic<K, V> {

    private final MemoryCacheImpl<K, V> memoryCache;
    private final FileCacheImpl<K, V> fileCache;
    private final Strategy strategy;
    private Map<K, Long> cashUsedInfo;

    public Cache(final int memoryCacheSize, final int fileCacheSize, final StrategyType strategy) throws IOException {
        this.memoryCache = new MemoryCacheImpl<>(memoryCacheSize);
        this.fileCache = new FileCacheImpl<>(fileCacheSize);
        this.strategy = setStrategy(strategy);
        this.cashUsedInfo = new HashMap<>(memoryCacheSize + fileCacheSize);
    }

    @Override
    public void put(K key, V value) {
        if (cacheHasPlace(memoryCache)) {
            memoryCache.put(key, value);
            System.out.println(format("%s updateCacheInfo in memory cache", String.valueOf(key)));
        } else if (cacheHasPlace(fileCache)) {
            fileCache.put(key, value);
            System.out.println(format("%s updateCacheInfo in file cache", String.valueOf(key)));
        } else {
            updateCache(key, value);
        }
        strategy.updateCacheInfo(key, cashUsedInfo);
    }

    @Override
    public V get(K key) {
        V result = null;
        if (memoryCache.cacheContainsKey(key)) {
            result = memoryCache.get(key);
        }
        if (fileCache.cacheContainsKey(key)) {
            result = fileCache.get(key);
        }
        if (Objects.nonNull(result)) {
            strategy.updateCacheInfo(key, cashUsedInfo);
        }
        return result;
    }

    @Override
    public void removeElement(K key) {
        if (memoryCache.cacheContainsKey(key)) {
            memoryCache.removeElement(key);
            System.out.println(format("%s removed from memory cache", String.valueOf(key)));
            cashUsedInfo.remove(key);
        }
        if (fileCache.cacheContainsKey(key)) {
            fileCache.removeElement(key);
            System.out.println(format("%s removed from file cache", String.valueOf(key)));
            cashUsedInfo.remove(key);
        }
        System.out.println(format("%s not in cache", String.valueOf(key)));
    }

    @Override
    public void clear() throws IOException {
        memoryCache.clear();
        fileCache.clear();
        cashUsedInfo.clear();
    }

    @Override
    public boolean cacheIsEmpty() {
        return memoryCache.cacheIsEmpty() && fileCache.cacheIsEmpty();
    }

    @Override
    public boolean hasPlace() {
        return memoryCache.hasPlace() || fileCache.hasPlace();
    }

    @Override
    public boolean cacheContainsKey(K key) {
        return memoryCache.cacheContainsKey(key) || fileCache.cacheContainsKey(key);
    }

    private boolean cacheHasPlace(CacheLogic cache) {
        return cache.cacheIsEmpty() || cache.hasPlace();
    }

    private void updateCache(K key, V value) {
        Optional.of((K) strategy.cacheElementForRemove(cashUsedInfo)).ifPresent(item -> {
            if (memoryCache.cacheContainsKey(item)) {
                memoryCache.removeElement(item);
                memoryCache.put(key, value);
                System.out.println(format("%s removed from memory cache", String.valueOf(item)));
                System.out.println(format("%s replaced in memory cache", String.valueOf(key)));
                cashUsedInfo.remove(item);
            }
            if (fileCache.cacheContainsKey(item)) {
                fileCache.removeElement(item);
                fileCache.put(key, value);
                System.out.println(format("%s removed from file cache", String.valueOf(item)));
                System.out.println(format("%s replaced in file cache", String.valueOf(key)));
                cashUsedInfo.remove(item);
            }
        });
    }

    private Strategy setStrategy(final StrategyType strategy) {
        if (strategy == StrategyType.LRU || Objects.isNull(strategy)) {
            return new LeastRecentlyUsedStrategyImpl();
        }
        if (strategy == StrategyType.MRU) {
            return new MostRecentlyUsedStrategyImpl();
        }
        return new LeastFrequentlyUsedStrategyImpl();
    }
}
