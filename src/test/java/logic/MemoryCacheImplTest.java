package logic;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MemoryCacheImplTest {

    private final int cacheSize = 4;
    private CacheLogic<String, Integer> cache;

    @Before
    public void before() throws Exception {
        cache = new MemoryCacheImpl<>(cacheSize);
    }

    @Test
    public void logicTest() throws Exception {
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        final String key1 = "key1";
        final String key2 = "key2";
        final String key3 = "key3";
        final String key4 = "key4";
        final int value1 = 1;

        cache.put(key1, value1);
        cache.put(key2, 2);
        cache.put(key3, 3);
        cache.put(key4, 4);

        assertFalse(cache.hasPlace());

        cache.removeElement(key2);

        assertFalse(cache.cacheContainsKey(key2));
        assertTrue(cache.hasPlace());
        assertEquals(value1, cache.get(key1).intValue());

        cache.clear();
        assertTrue(cache.cacheIsEmpty());

        assertNull(cache.get(key1));
    }

}