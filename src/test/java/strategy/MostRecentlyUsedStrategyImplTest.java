package strategy;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class MostRecentlyUsedStrategyImplTest {
    private Map<String, Long> cache;
    private Strategy<String> strategy;
    private final String key1 = "key1";
    private final String key2 = "key2";
    private final String key3 = "key3";
    private final String key4 = "key4";

    @Before
    public void before() {
        cache = new HashMap<>();
        strategy = new MostRecentlyUsedStrategyImpl<>();
    }

    @Test
    public void updateCacheInfo() throws Exception {
        assertTrue(cache.isEmpty());

        strategy.updateCacheInfo(key1, cache);

        assertFalse(cache.isEmpty());
        assertTrue(cache.containsKey(key1));

        strategy.updateCacheInfo(key2, cache);
        strategy.updateCacheInfo(key3, cache);
        strategy.updateCacheInfo(key4, cache);
        strategy.updateCacheInfo(key1, cache);

        assertTrue(cache.size() == 1);
        assertTrue(cache.containsKey(key1));
    }

    @Test
    public void cacheElementForRemove() throws Exception {
        assertTrue(cache.isEmpty());
        strategy.updateCacheInfo(key1, cache);
        strategy.updateCacheInfo(key4, cache);
        strategy.updateCacheInfo(key4, cache);
        strategy.updateCacheInfo(key2, cache);
        strategy.updateCacheInfo(key1, cache);

        assertEquals(key1, strategy.cacheElementForRemove(cache));
    }
}