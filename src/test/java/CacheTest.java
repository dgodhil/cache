import logic.CacheLogic;
import org.junit.Test;
import strategy.StrategyType;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CacheTest {

    private final int memoryCacheSize = 4;
    private final int fileCacheSize = 2;
    private CacheLogic<String, Integer> cache;

    private final String key1 = "key1";
    private final String key2 = "key2";
    private final String key3 = "key3";
    private final String key4 = "key4";
    private final String key5 = "key5";
    private final String key6 = "key6";
    private final String key7 = "key7";
    private final String key8 = "key8";
    private final int value1 = 1;
    private final int value2 = 2;
    private final int value3 = 3;
    private final int value4 = 4;
    private final int value5 = 5;
    private final int value6 = 6;
    private final int value7 = 7;
    private final int value8 = 8;

    @Test
    public void LRU_Strategy() throws Exception {
        cache = new Cache<>(memoryCacheSize, fileCacheSize, StrategyType.LRU);
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key1, value1);
        assertTrue(cache.cacheContainsKey(key1));

        cache.put(key2, value2);

        assertFalse(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key3, value3);
        cache.put(key4, value4);
        cache.put(key5, value5);
        cache.put(key6, value6);

        assertFalse(cache.hasPlace());

        cache.put(key7, value7);

        assertNull(cache.get(key1));

        cache.get(key2);
        cache.put(key8, value8);
        assertNull(cache.get(key3));

        assertFalse(cache.hasPlace());

        cache.removeElement(key6);
        assertNull(cache.get(key6));
        assertTrue(cache.hasPlace());

        cache.get(key2);
        cache.get(key3);
        cache.get(key4);
        cache.put(key1, value1);
        cache.get(key1);
        assertFalse(cache.hasPlace());

        cache.put(key6, value6);
        assertNull(cache.get(key5));

        cache.clear();
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());
    }

    @Test
    public void LFU_Strategy() throws Exception {
        cache = new Cache<>(memoryCacheSize, fileCacheSize, StrategyType.LRU);
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key1, value1);
        assertTrue(cache.cacheContainsKey(key1));

        cache.put(key2, value2);

        assertFalse(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key3, value3);
        cache.put(key4, value4);
        cache.put(key5, value5);
        cache.put(key6, value6);

        assertFalse(cache.hasPlace());

        cache.put(key7, value7);
        assertNull(cache.get(key1));

        cache.get(key2);
        cache.put(key8, value8);
        assertNull(cache.get(key3));

        assertFalse(cache.hasPlace());

        cache.removeElement(key6);
        assertTrue(cache.hasPlace());
        assertNull(cache.get(key6));

        cache.get(key2);
        cache.get(key3);
        cache.get(key4);
        cache.put(key1, value1);
        cache.get(key1);
        assertFalse(cache.hasPlace());

        cache.put(key6, value6);
        assertNull(cache.get(key5));

        cache.removeElement(key2);
        assertNull(cache.get(key2));

        cache.clear();
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());
    }

    @Test
    public void MRU_Strategy() throws Exception {
        cache = new Cache<>(memoryCacheSize, fileCacheSize, StrategyType.MRU);
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key1, value1);
        assertTrue(cache.cacheContainsKey(key1));

        cache.put(key2, value2);

        assertFalse(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key3, value3);
        cache.put(key4, value4);
        cache.put(key5, value5);
        cache.put(key6, value6);

        assertFalse(cache.hasPlace());

        cache.put(key7, value7);
        assertNull(cache.get(key6));

        cache.get(key2);
        cache.put(key8, value8);
        assertNull(cache.get(key2));

        assertFalse(cache.hasPlace());

        cache.removeElement(key5);
        assertNull(cache.get(key5));
        assertTrue(cache.hasPlace());

        cache.get(key2);
        cache.put(key1, value1);
        cache.get(key8);
        assertFalse(cache.hasPlace());

        cache.put(key6, value6);
        assertNull(cache.get(key8));

        cache.clear();
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());
    }


    @Test
    public void tryRemoveElementNotInCache() throws Exception {
        cache = new Cache<>(memoryCacheSize, fileCacheSize, null);
        assertTrue(cache.cacheIsEmpty());
        assertTrue(cache.hasPlace());

        cache.put(key1, value1);
        assertTrue(cache.cacheContainsKey(key1));

        cache.removeElement(key2);
    }
}